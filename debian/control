Source: usbguard
Section: utils
Priority: optional
Maintainer: Muri Nicanor <muri@immerda.ch>
Build-Depends: aspell,
               asciidoctor,
               bash-completion,
               catch,
               debhelper (>=11),
               dh-exec (>=0.3),
               libaudit-dev,
               libcap-ng-dev,
               libdbus-1-dev,
               libdbus-glib-1-dev,
               libglib2.0-dev,
               libpolkit-gobject-1-dev,
               libprotobuf-dev,
               libqb-dev,
               libqt5svg5-dev,
               libseccomp-dev,
               libsodium-dev,
               libudev-dev,
               libxml2-utils,
               locales-all,
               pandoc,
               pegtl-dev,
               pkg-config,
               protobuf-compiler,
               qtbase5-dev,
               qtbase5-dev-tools,
               qtchooser,
               qttools5-dev-tools,
               systemd,
               xsltproc
Standards-Version: 4.1.3
Homepage: https://dkopecek.github.io/usbguard/
Vcs-Git: https://salsa.debian.org/muri-guest/usbguard.git
Vcs-Browser: https://salsa.debian.org/muri-guest/usbguard

Package: libusbguard0
Section: libs
Architecture: linux-any
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: USB device authorization policy framework - shared library
 The USBGuard software framework helps to protect your computer against rogue
 USB devices (a.k.a. BadUSB) by implementing basic whitelisting and blacklisting
 capabilities based on device attributes.
 .
 This package contains the shared library

Package: usbguard
Architecture: linux-any
Depends: ${misc:Depends}, ${shlibs:Depends}, dbus
Description: USB device authorization policy framework
 The USBGuard software framework helps to protect your computer against rogue
 USB devices (a.k.a. BadUSB) by implementing basic whitelisting and blacklisting
 capabilities based on device attributes.

Package: usbguard-applet-qt
Architecture: linux-any
Depends: usbguard, ${misc:Depends}, ${shlibs:Depends}
Description: USB device authorization policy framework - qt applet
 The USBGuard software framework helps to protect your computer against rogue
 USB devices (a.k.a. BadUSB) by implementing basic whitelisting and blacklisting
 capabilities based on device attributes.
 .
 This package contains the qt-applet for controlling usbguard.
